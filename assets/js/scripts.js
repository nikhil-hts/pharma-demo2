(function ($) {
    "use strict";

    $(document).on('scroll', function () {
        if (window.pageYOffset >= 20) {
            $('.header-area').addClass('sticky-header')
        } else {
            $('.header-area').removeClass('sticky-header')
        }
    })
    let headerHeight = $('.header-area').innerHeight()
    $('.hero-area').css("padding-top", headerHeight)

    $(document).ready(function () {
        $('a[href*=#]').bind('click', function (e) {
            e.preventDefault();
            var target = $(this).attr("href");
            $('html, body').stop().animate({
                scrollTop: $(target).offset().top
            }, 600, function () {
                location.hash = target;
            });

            return false;
        });
    });

    $(window).scroll(function () {
        var scrollDistance = $(window).scrollTop();
        $('section').each(function (i) {
            if ($(this).position().top <= scrollDistance) {
                $('.mainmenu li a.active').removeClass('active');
                $('.mainmenu li a').eq(i).addClass('active');
            }
        });
    }).scroll();
})(jQuery);
